# Documentation

A library to control a Serializer 3.0 from RoboticsConnection.com Datasheet: https://os.mbed.com/media/uploads/aswild/serializer_3.0_userguide.pdf

The Serializer can be connected with a hardware/software serial port.

Innovation Crunch Lab UTBM 2018

 * **Author:** Arthur KLIPFEL

```c++
bool begin(uint32_t _baudrate=19200)
```

Start communicate with the serializer.

 * **Parameters:** `_baudrate` — the baudrate of set serial port
 * **Returns:** true if the library have detected the board

```c++
bool begin(HardwareSerial *_serial,uint32_t _baudrate=19200,unsigned long _timeout=1500)
```

Start communicate with the serializer.

 * **Parameters:**
   * `_serial` — a pointer to a hardware serial port
   * `_baudrate` — the baudrate of set serial port
   * `_timeout` — time in milliseconds
 * **Returns:** true if the library have detected the board

```c++
bool begin(SoftwareSerial *_serial,uint32_t _baudrate=19200,unsigned long _timeout=1500)
```

Start communicate with the serializer.

 * **Parameters:**
   * `_serial` — a pointer to a software serial port
   * `_baudrate` — the baudrate of set serial port
   * `_timeout` — time in milliseconds
 * **Returns:** true if the library have detected the board

```c++
void setTimeout(unsigned long _timeout)
```

Set the timeout value for all command to the serializer.

 * **Parameters:** `_timeout` — time in milliseconds

```c++
bool reset(unsigned long _timeout=1500)
```

Reset the Serializer.

 * **Parameters:** `_timeout` — time in milliseconds
 * **Returns:** true if it work

```c++
String getVersion()
```

Get the version of the connected Serializer

 * **Returns:** the version of the serializer

```c++
bool blink(int frequency=0)
```

Make a serializer's led blink

 * **Parameters:** `frequency` — from 0 to 100, 0 turn the led off
 * **Returns:** true if it work

```c++
bool setEncoderMode(EncoderMode mode)
```

Set encoder's mode.

 * **Parameters:** `mode` — SINGLE or QUADRATURE
 * **Returns:** true if it work

```c++
EncoderMode getEncoderMode()
```

Get encoder's mode.

 * **Returns:** SINGLE, QUADRATURE or ENCODER_ERROR if the library can't read the configuration

```c++
int getEncoder(Encoder encoder)
```

Get encoder's value.

 * **Parameters:** `encoder` — the side of the encoder LEFT or RIGHT
 * **Returns:** a integer representing the steps counted by the encoder

```c++
bool getEncoders(int &right,int &left)
```

Get encoder's value. The function provide value for both side.

 * **Parameters:**
   * `right` — right value
   * `left` — left value
 * **Returns:** true if it work

```c++
bool clearEncoder(Encoder encoder)
```

Clear encoder's value.

 * **Parameters:** `encoder` — the side of the encoder LEFT or RIGHT
 * **Returns:** true if it work

```c++
bool clearEncoders()
```

Clear encoder's value. The function clear both side.

 * **Returns:** true if it work

```c++
bool setBaudrate(uint32_t baudrate)
```

Set the baudrate of the serializer. This function also change the arduino board baudrate.

 * **Parameters:** `baudrate` — the value of the new baudrate
 * **Returns:** true if it work

```c++
uint32_t getBaudrate()
```

Get the baudrate of the serializer.

 * **Returns:** baudrate value

```c++
bool setUnits(Units units)
```

Set the units of the serializer.

 * **Parameters:** `units` — METRIC or IMPERIAL
 * **Returns:** true if it work

```c++
Units getUnits()
```

Get the units of the serializer

 * **Returns:** METRIC, IMPERIAL or UNITS_ERROR if the library can't read the configuration

```c++
bool setIO(int pin,bool value)
```

Set a GPIO pin value

 * **Parameters:**
   * `pin` — id of the pin
   * `value` — value of the pin (HIGH or LOW)
 * **Returns:** true if it work

```c++
bool getIO(int pin)
```

Get a GPIO pin value

 * **Parameters:** `pin` — id of the pin
 * **Returns:** value of the pin (HIGH or LOW)

```c++
int getMaxez1(int trigger,int output)
```

Get the value of a Maxez1 sensor

 * **Parameters:**
   * `trigger` — id of the trigger pin
   * `output` — id of the output pin
 * **Returns:** distance

```c++
bool setMotorSpeed(Encoder side,int speed)
```

Set the motor's speed.

 * **Parameters:**
   * `side` — the side of the motor LEFT or RIGHT
   * `speed` — speed
 * **Returns:** true if it work

```c++
bool setMotorsSpeed(int speedRight,int speedLeft)
```

Set the motor's speed. Both motor.

 * **Parameters:**
   * `speedRight` — speed of the right motor
   * `speedLeft` — speed of the left motor
 * **Returns:** true if it work

```c++
bool stopMotors()
```

Turn off motor

 * **Returns:** true if it work

```c++
bool setSpeedPID(PID pid)
```

Update PID parameters value (for speed control)

 * **Parameters:** `pid` — new parameters
 * **Returns:** true if it work

```c++
PID getSpeedPID()
```

Get speed pid parameters

 * **Returns:** current parameters

```c++
bool move(Encoder side,int distance,int speed)
```

Rotate a wheel

 * **Parameters:**
   * `side` — the side of the motor LEFT or RIGHT
   * `distance` — distance in encoder ticks
   * `speed` — top speed
 * **Returns:** true if it work

```c++
bool move(int distanceRight,int speedRight,int distanceLeft,int speedLeft)
```

Rotate a wheel

 * **Parameters:**
   * `distanceRight` — distance of the right wheel in encoder ticks
   * `speedRight` — top speed of the right wheel
   * `distanceLeft` — distance of the left wheel in encoder ticks
   * `speedLeft` — top speed of the left wheel
 * **Returns:** true if it work

```c++
bool setPositionPID(PID pid)
```

Update PID parameters value (for position control)

 * **Parameters:** `pid` — new parameters
 * **Returns:** true if it work

```c++
PID getPositionPID()
```

Get position pid parameters

 * **Returns:** current parameters

```c++
bool resetPositionPID(Kit kit=STINGER)
```

Backup factory parameter's for PID controler

 * **Parameters:** `kit` — with robot
 * **Returns:** true if it work

```c++
bool isPIDBusy()
```

Did the robot finish to move?

 * **Returns:** true if it work

```c++
bool setPWM(Encoder side,int pwm)
```

Set the pwm output of a motor

 * **Parameters:**
   * `side` — the side of the motor LEFT or RIGHT
   * `pwm` — power give to the motor between -100 and 100
 * **Returns:** true if it work

```c++
bool setPWM(Encoder side,int pwm,int ratio)
```

Set the pwm output of a motor

 * **Parameters:**
   * `side` — the side of the motor LEFT or RIGHT
   * `pwm` — power give to the motor between -100 and 100
   * `ratio` — ratio describe in serializer's datasheet page 25
 * **Returns:** true if it work

```c++
bool setPWM(int pwmRight,int pwmLeft)
```

Set the pwm output of a motor on both side

 * **Parameters:**
   * `pwmRight` — power give to the right motor between -100 and 100
   * `pwmLeft` — power give to the left motor between -100 and 100
 * **Returns:** true if it work

```c++
bool setPWM(int pwmRight,int pwmLeft,int ratio)
```

Set the pwm output of a motor on both side

 * **Parameters:**
   * `pwmRight` — power give to the right motor between -100 and 100
   * `pwmLeft` — power give to the left motor between -100 and 100
   * `ratio` — ratio describe in serializer's datasheet page 25
 * **Returns:** true if it work

```c++
int getSensor(int id)
```

Get the value of an analog sensor of the serializer

 * **Parameters:** `id` — id of the pin
 * **Returns:** value between 0 and 1023

```c++
bool setServo(int id,int position)
```

Set a servo position

 * **Parameters:**
   * `id` — id of the servo (describe p 28 of the datasheet)
   * `position` — integer from -99 to 100, position -100 disable the servo
 * **Returns:** true if it work

```c++
bool setServoGPIO(int pin,int position)
```

Set a servo position

 * **Parameters:**
   * `pin` — id of the servo's pin
   * `position` — integer from -99 to 100, position -100 disable the servo
 * **Returns:** true if it work

```c++
int getSRF04(int trigger,int output)
```

Get the value of a SRF04 sensor

 * **Parameters:**
   * `trigger` — id of the trigger pin
   * `output` — id of the output pin
 * **Returns:** distance

```c++
bool getSpeed(int &right,int &left)
```

Get encoder's speed. The function provide value for both side.

 * **Parameters:**
   * `right` — right speed
   * `left` — left speed
 * **Returns:** true if it work

```c++
bool restore(unsigned long _timeout=1500)
```

Restore factory parameters

 * **Parameters:** `_timeout` — time in milliseconds
 * **Returns:** true if it work