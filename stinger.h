#ifndef __STINGER_H__
#define __STINGER_H__

/**
 * A library to control a Serializer 3.0 from RoboticsConnection.com
 * Datasheet: https://os.mbed.com/media/uploads/aswild/serializer_3.0_userguide.pdf
 * 
 * The Serializer can be connected with a hardware serial port or a software one.
 * 
 * Innovation Crunch Lab UTBM
 * @author
 * Arthur KLIPFEL 2018
 */

#include <Arduino.h>
#include <inttypes.h>
#include <Stream.h>
#include <SoftwareSerial.h>

#define H_BRIDGES 10
#define DISABLE -100

typedef enum{
	SINGLE=0,
	QUADRATURE=1,
	ENCODER_ERROR=2
}EncoderMode;

typedef enum{
	METRIC=0,
	IMPERIAL=1,
	UNITS_ERROR=2
}Units;

typedef enum{
	LEFT=1,
	RIGHT=2
}Encoder;

typedef struct{
	int p;
	int i;
	int d;
	union{
		int l;
		int a;
	};
}PID;

typedef enum{
	STINGER='s',
	TRAXER='t'
}Kit;

bool operator==(const PID &a,const PID &b);
bool operator!=(const PID &a,const PID &b);

class StingerClass{
public:

	StingerClass();
	/**
	 * Start communicate with the serializer.
	 * @param _baudrate   the baudrate of set serial port
	 * @return true if the library have detected the board
	 */
	bool begin(uint32_t _baudrate=19200);
	/**
	 * Start communicate with the serializer.
	 * @param _serial     a pointer to a hardware serial port
	 * @param _baudrate   the baudrate of set serial port
	 * @param _timeout    time in milliseconds
	 * @return true if the library have detected the board
	 */
	bool begin(HardwareSerial *_serial,uint32_t _baudrate=19200,unsigned long _timeout=1500);
	/**
	 * Start communicate with the serializer.
	 * @param _serial     a pointer to a software serial port
	 * @param _baudrate   the baudrate of set serial port
	 * @param _timeout    time in milliseconds
	 * @return true if the library have detected the board
	 */
	bool begin(SoftwareSerial *_serial,uint32_t _baudrate=19200,unsigned long _timeout=1500);
	/**
	 * Set the timeout value for all command to the serializer.
	 * @param _timeout    time in milliseconds
	 */
	void setTimeout(unsigned long _timeout);
	/**
	 * Reset the Serializer.
	 * @param _timeout    time in milliseconds
	 * @return true if it work
	 */
	bool reset(unsigned long _timeout=1500);
	/**
	 * Get the version of the connected Serializer
	 * @return the version of the serializer
	 */
	String getVersion();
	/**
	 * Make a serializer's led blink
	 * @param frequency    from 0 to 100, 0 turn the led off
	 * @return true if it work
	 */
	bool blink(int frequency=0);//led 2 don't work
	/**
	 * Set encoder's mode.
	 * @param mode    SINGLE or QUADRATURE
	 * @return true if it work
	 */
	bool setEncoderMode(EncoderMode mode);
	/**
	 * Get encoder's mode.
	 * @return SINGLE, QUADRATURE or ENCODER_ERROR if the library can't read the configuration
	 */
	EncoderMode getEncoderMode();
	/**
	 * Get encoder's value.
	 * @param encoder    the side of the encoder LEFT or RIGHT
	 * @return a integer representing the steps counted by the encoder
	 */
	int getEncoder(Encoder encoder);
	/**
	 * Get encoder's value. The function provide value for both side.
	 * @param right    right value
	 * @param left     left value
	 * @return true if it work
	 */
	bool getEncoders(int &right,int &left);
	/**
	 * Clear encoder's value.
	 * @param encoder    the side of the encoder LEFT or RIGHT
	 * @return true if it work
	 */
	bool clearEncoder(Encoder encoder);
	/**
	 * Clear encoder's value. The function clear both side.
	 * @return true if it work
	 */
	bool clearEncoders();
	/**
	 * Set the baudrate of the serializer. This function also change the arduino board baudrate.
	 * @param baudrate    the value of the new baudrate
	 * @return true if it work
	 */
	bool setBaudrate(uint32_t baudrate);
	/**
	 * Get the baudrate of the serializer.
	 * @return baudrate value
	 */
	uint32_t getBaudrate();
	/**
	 * Set the units of the serializer.
	 * @param units    METRIC or IMPERIAL
	 * @return true if it work
	 */
	bool setUnits(Units units);
	/**
	 * Get the units of the serializer
	 * @return METRIC, IMPERIAL or UNITS_ERROR if the library can't read the configuration
	 */
	Units getUnits();
	/**
	 * Set a GPIO pin value
	 * @param pin      id of the pin
	 * @param value    value of the pin (HIGH or LOW)
	 * @return true if it work
	 */
	bool setIO(int pin,bool value);
	/**
	 * Get a GPIO pin value
	 * @param pin      id of the pin
	 * @return value of the pin (HIGH or LOW)
	 */
	bool getIO(int pin);
	/**
	 * Get the value of a Maxez1 sensor
	 * @param trigger     id of the trigger pin
	 * @param output      id of the output pin
	 * @return distance
	 */
	int getMaxez1(int trigger,int output);
	/**
	 * Set the motor's speed.
	 * @param side        the side of the motor LEFT or RIGHT
	 * @param speed       speed
	 * @return true if it work
	 */
	bool setMotorSpeed(Encoder side,int speed);
	/**
	 * Set the motor's speed. Both motor.
	 * @param speedRight      speed of the right motor
	 * @param speedLeft       speed of the left motor
	 * @return true if it work
	 */
	bool setMotorsSpeed(int speedRight,int speedLeft);
	/**
	 * Turn off motor
	 * @return true if it work
	 */
	bool stopMotors();
	/**
	 * Update PID parameters value (for speed control)
	 * @param pid       new parameters
	 * @return true if it work
	 */
	bool setSpeedPID(PID pid);
	/**
	 * Get speed pid parameters
	 * @return current parameters
	 */
	PID getSpeedPID();
	/**
	 * Rotate a wheel
	 * @param side        the side of the motor LEFT or RIGHT
	 * @param distance    distance in encoder ticks
	 * @param speed       top speed
	 * @return true if it work
	 */
	bool move(Encoder side,int distance,int speed);
	/**
	 * Rotate a wheel
	 * @param distanceRight  distance of the right wheel in encoder ticks
	 * @param speedRight     top speed of the right wheel
	 * @param distanceLeft   distance of the left wheel in encoder ticks
	 * @param speedLeft      top speed of the left wheel
	 * @return true if it work
	 */
	bool move(int distanceRight,int speedRight,int distanceLeft,int speedLeft);
	/**
	 * Update PID parameters value (for position control)
	 * @param pid       new parameters
	 * @return true if it work
	 */
	bool setPositionPID(PID pid);
	/**
	 * Get position pid parameters
	 * @return current parameters
	 */
	PID getPositionPID();
	/**
	 * Backup factory parameter's for PID controler
	 * @param kit       with robot
	 * @return true if it work
	 */
	bool resetPositionPID(Kit kit=STINGER);
	/**
	 * Did the robot finish to move?
	 * @return true if it work
	 */
	bool isPIDBusy();
	/**
	 * Set the pwm output of a motor
	 * @param side       the side of the motor LEFT or RIGHT
	 * @param pwm        power give to the motor between -100 and 100
	 * @return true if it work
	 */
	bool setPWM(Encoder side,int pwm);
	/**
	 * Set the pwm output of a motor
	 * @param side       the side of the motor LEFT or RIGHT
	 * @param pwm        power give to the motor between -100 and 100
	 * @param ratio      ratio describe in serializer's datasheet page 25
	 * @return true if it work
	 */
	bool setPWM(Encoder side,int pwm,int ratio);
	/**
	 * Set the pwm output of a motor on both side
	 * @param pwmRight    power give to the right motor between -100 and 100
	 * @param pwmLeft     power give to the left motor between -100 and 100
	 * @return true if it work
	 */
	bool setPWM(int pwmRight,int pwmLeft);
	/**
	 * Set the pwm output of a motor on both side
	 * @param pwmRight   power give to the right motor between -100 and 100
	 * @param pwmLeft    power give to the left motor between -100 and 100
	 * @param ratio      ratio describe in serializer's datasheet page 25
	 * @return true if it work
	 */
	bool setPWM(int pwmRight,int pwmLeft,int ratio);
	/**
	 * Get the value of an analog sensor of the serializer
	 * @param id      id of the pin
	 * @return value between 0 and 1023
	 */
	int getSensor(int id);
	/**
	 * Set a servo position
	 * @param id         id of the servo (describe p 28 of the datasheet)
	 * @param position   integer from -99 to 100, position -100 disable the servo
	 * @return true if it work
	 */
	bool setServo(int id,int position);
	/**
	 * Set a servo position
	 * @param pin        id of the servo's pin
	 * @param position   integer from -99 to 100, position -100 disable the servo
	 * @return true if it work
	 */
	bool setServoGPIO(int pin,int position);
	/**
	 * Get the value of a SRF04 sensor
	 * @param trigger     id of the trigger pin
	 * @param output      id of the output pin
	 * @return distance
	 */
	int getSRF04(int trigger,int output);
	/**
	 * Get encoder's speed. The function provide value for both side.
	 * @param right    right speed
	 * @param left     left speed
	 * @return true if it work
	 */
	bool getSpeed(int &right,int &left);
	/**
	 * Restore factory parameters
	 * @param _timeout    time in milliseconds
	 * @return true if it work
	 */
	bool restore(unsigned long _timeout=1500);
protected:
	inline PID getPID(String name);
	void clearBuffer();
	String readBuffer();
	Stream *serial;
	bool hardwareSerial;
	unsigned long timeout;
private:
};

extern StingerClass Stinger;

#endif