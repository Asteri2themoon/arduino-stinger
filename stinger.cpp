#include "stinger.h"

bool operator==(const PID &a,const PID &b){
	return a.p==b.p && a.i==b.i && a.d==b.d && a.l==b.l;
}

bool operator!=(const PID &a,const PID &b){
	return a.p!=b.p || a.i!=b.i || a.d!=b.d || a.l!=b.l;
}

StingerClass Stinger;

StingerClass::StingerClass(){
	StingerClass::serial=&Serial;
	timeout=1000;
	hardwareSerial=true;
}

bool StingerClass::begin(uint32_t _baudrate){
	return StingerClass::begin(&Serial,_baudrate);
}

bool StingerClass::begin(HardwareSerial *_serial,uint32_t _baudrate,unsigned long _timeout){
	_serial->begin(_baudrate);
	hardwareSerial=true;
	StingerClass::serial=_serial;
	return StingerClass::reset(_timeout);
}

bool StingerClass::begin(SoftwareSerial *_serial,uint32_t _baudrate,unsigned long _timeout){
	_serial->begin(_baudrate);
	hardwareSerial=false;
	StingerClass::serial=_serial;
	return StingerClass::reset(_timeout);
}

void StingerClass::setTimeout(unsigned long _timeout){
	timeout=_timeout;
}

bool StingerClass::reset(unsigned long _timeout){
	const char *validReply="Serializer, firmware 3.0.0 Copyright 2006-2010, RoboticsConnection.com\r\n>";
	const int bufferSize=128;
	char buffer[bufferSize];
	int i=0,len=strlen(validReply);
	long end;
	bool eol=false;
	
	clearBuffer();
	end=millis()+_timeout;
	StingerClass::serial->write("reset\r");
	while(millis()<end && i<bufferSize){
		if(StingerClass::serial->available()){
			buffer[i++]=StingerClass::serial->read();
		}
	}
	buffer[i]='\0';
	
	return i>=len && strcmp(&buffer[i-len],validReply)==0;
}

String StingerClass::getVersion(){
	char cIn;
	bool done=false;
	String version="";
	
	clearBuffer();
	StingerClass::serial->write("fw\r");
	
	version=readBuffer();
	
	clearBuffer();
	return version;
}

bool StingerClass::blink(int frequency){
	String result;
	if(frequency<0) frequency=0;
	else if(frequency>100) frequency=100;
	
	clearBuffer();
	StingerClass::serial->print("blink 1:"+String(frequency)+"\r");
	
	result=readBuffer();
	return result=="ACK";
}

bool StingerClass::setEncoderMode(EncoderMode mode){
	String result;
	clearBuffer();
	StingerClass::serial->print("cfg enc "+String((int)mode)+"\r");
	
	result=readBuffer();
	return result=="ACK";
}

EncoderMode StingerClass::getEncoderMode(){
	String result;
	clearBuffer();
	StingerClass::serial->print("cfg enc\r");
	
	result=readBuffer();
	switch(result[0]){
	case '0':
		return SINGLE;
	case '1':
		return QUADRATURE;
	default:
		return ENCODER_ERROR;
	}
}

int StingerClass::getEncoder(Encoder encoder){
	String result;
	clearBuffer();
	StingerClass::serial->print("getenc "+String((int)encoder)+"\r");
	
	result=readBuffer();
	return result.toInt();
}

bool StingerClass::getEncoders(int &right,int &left){
	String result;
	clearBuffer();
	StingerClass::serial->print("getenc 1 2\r");
	
	result=readBuffer();
	int space=result.indexOf(' ');
	if(space!=-1){
		result[space]='\0';
		left=atoi(&result[0]);
		right=atoi(&result[space+1]);
		return true;
	}
	return false;
}

bool StingerClass::clearEncoder(Encoder encoder){
	String result;
	clearBuffer();
	StingerClass::serial->print("clrenc "+String((int)encoder)+"\r");
	
	result=readBuffer();
	return result=="ACK";
}

bool StingerClass::clearEncoders(){
	String result;
	clearBuffer();
	StingerClass::serial->print("clrenc 1 2\r");
	
	result=readBuffer();
	return result=="ACK";
}

bool StingerClass::setBaudrate(uint32_t baudrate){
	String result;
	clearBuffer();
	StingerClass::serial->print("cfg baud "+String(baudrate)+"\r");
	
	if(hardwareSerial){
		((HardwareSerial*)serial)->end();
		((HardwareSerial*)serial)->begin(baudrate);
	}else{
		//((SoftwareSerial*)serial)->end();
		((SoftwareSerial*)serial)->begin(baudrate);
	}
	delay(100);
	return StingerClass::getBaudrate()==baudrate;
}

uint32_t StingerClass::getBaudrate(){
	String result;
	clearBuffer();
	StingerClass::serial->print("cfg baud\r");
	
	result=readBuffer();
	return result.toInt();
}

bool StingerClass::setUnits(Units units){
	String result;
	clearBuffer();
	StingerClass::serial->print("cfg units "+String((int)units)+"\r");
	
	result=readBuffer();
	return result=="ACK";
}

Units StingerClass::getUnits(){
	String result;
	clearBuffer();
	StingerClass::serial->print("cfg units\r");
	
	result=readBuffer();
	switch(result[0]){
	case '0':
		return METRIC;
	case '1':
		return IMPERIAL;
	default:
		return UNITS_ERROR;
	}
}

bool StingerClass::setIO(int pin,bool value){
	String result;
	
	if(pin<0 || pin>12){
		return false;
	}
	
	clearBuffer();
	StingerClass::serial->print("setio "+String(pin)+":"+(value?"1":"0")+"\r");
	
	result=readBuffer();
	return result=="ACK";
}

bool StingerClass::getIO(int pin){
	String result;
	
	if(pin<0 || pin>12){
		return false;
	}
	
	clearBuffer();
	StingerClass::serial->print("getio "+String(pin)+"\r");
	
	result=readBuffer();
	switch(result[0]){
	case '0':
		return false;
	case '1':
		return true;
	default:
		return false;
	}
}

int StingerClass::getMaxez1(int trigger,int output){
	String result;
	
	if(trigger<0 || trigger>12){
		return 0;
	}
	if(output<0 || output>12){
		return 0;
	}
	
	clearBuffer();
	StingerClass::serial->print("maxez1 "+String(trigger)+" "+String(output)+"\r");
	
	result=readBuffer();
	return result.toInt();
}

bool StingerClass::setMotorSpeed(Encoder side,int speed){
	String result;
	
	clearBuffer();
	StingerClass::serial->print("mogo "+String((int)side)+":"+String(speed)+"\r");
	
	result=readBuffer();
	return result=="ACK";
}

bool StingerClass::setMotorsSpeed(int speedRight,int speedLeft){
	String result;
	
	clearBuffer();
	StingerClass::serial->print("mogo 1:"+String(speedLeft)+" 2:"+String(speedRight)+"\r");
	
	result=readBuffer();
	return result=="ACK";
}

bool StingerClass::stopMotors(){
	String result;
	
	clearBuffer();
	StingerClass::serial->print("stop\r");
	
	result=readBuffer();
	return result=="ACK";
}

bool StingerClass::setSpeedPID(PID pid){
	String result;
	
	clearBuffer();
	StingerClass::serial->print("vpid "+String(pid.p)+":"+String(pid.i)+":"+String(pid.d)+":"+String(pid.l)+"\r");
	
	result=readBuffer();
	return result=="ACK";
}


PID StingerClass::getPID(String name){
	String result;
	char parameter;
	String value;
	int i;
	PID pid;
	
	clearBuffer();
	StingerClass::serial->print(name+"\r");
	
	result=readBuffer();
	value="";
	for(i=0;i<=result.length();i++){//need i<=result.length() to update L parameter
		switch(result[i]){
		case '0'...'9':
			value+=result[i];
			break;
		case 'P':
		case 'I':
		case 'D':
		case 'L':
		case 'A':
			parameter=result[i];
			break;
		case ' ':
		case '\0':
			switch(parameter){
			case 'P':
				pid.p=value.toInt();
				break;
			case 'I':
				pid.i=value.toInt();
				break;
			case 'D':
				pid.d=value.toInt();
				break;
			case 'L':
				pid.l=value.toInt();
				break;
			case 'A':
				pid.a=value.toInt();
				break;
			default:
				break;
			}
			value="";
			parameter='\0';
			break;
		default:
			break;
		}
	}
	return pid;
}

PID StingerClass::getSpeedPID(){
	return StingerClass::getPID("vpid");
}

bool StingerClass::move(Encoder side,int distance,int speed){
	String result;
	
	clearBuffer();
	StingerClass::serial->print("digo "+String((int)side)+":"+String(distance)+":"+String(speed)+"\r");
	
	result=readBuffer();
	return result=="ACK";
}

bool StingerClass::move(int distanceRight,int speedRight,int distanceLeft,int speedLeft){
	String result;
	
	clearBuffer();
	StingerClass::serial->print("digo 1:"+String(distanceLeft)+":"+String(speedLeft)+" 2:"+String(distanceRight)+":"+String(speedRight)+"\r");
	
	result=readBuffer();
	return result=="ACK";
}

bool StingerClass::setPositionPID(PID pid){
	String result;
	
	clearBuffer();
	StingerClass::serial->print("dpid "+String(pid.p)+":"+String(pid.i)+":"+String(pid.d)+":"+String(pid.a)+"\r");
	
	result=readBuffer();
	return result=="ACK";
}

PID StingerClass::getPositionPID(){
	return StingerClass::getPID("dpid");
}

bool StingerClass::resetPositionPID(Kit kit){
	String result;
	
	clearBuffer();
	StingerClass::serial->print("rpid "+String((char)kit)+"\r");
	
	result=readBuffer();
	return result=="ACK";
}

bool StingerClass::isPIDBusy(){
	String result;
	
	clearBuffer();
	StingerClass::serial->print("pids\r");
	
	result=readBuffer();
	return result[0]=='1';
}

bool StingerClass::setPWM(Encoder side,int pwm){
	String result;
	
	if(pwm>100 || pwm<-100){
		//Serial.println("range error "+String(pwm));
		return false;
	}
	
	clearBuffer();
	StingerClass::serial->print("pwm "+String((int)side)+":"+String(pwm)+"\r");
	
	result=readBuffer();
	return result=="ACK";
}

bool StingerClass::setPWM(Encoder side,int pwm,int ratio){
	String result;
	
	if(pwm>100 || pwm<-100){
		return false;
	}
	
	clearBuffer();
	StingerClass::serial->print("pwm r:"+String(ratio)+" "+String((int)side)+":"+String(pwm)+"\r");
	
	result=readBuffer();
	return result=="ACK";
}

bool StingerClass::setPWM(int pwmRight,int pwmLeft){
	String result;
	
	if(pwmRight>100 || pwmRight<-100){
		return false;
	}
	
	if(pwmLeft>100 || pwmLeft<-100){
		return false;
	}
	
	clearBuffer();
	StingerClass::serial->print("pwm 1:"+String(pwmLeft)+" 2:"+String(pwmRight)+"\r");
	
	result=readBuffer();
	return result=="ACK";
}

bool StingerClass::setPWM(int pwmRight,int pwmLeft,int ratio){
	String result;
	
	if(pwmRight>100 || pwmRight<-100){
		return false;
	}
	
	if(pwmLeft>100 || pwmLeft<-100){
		return false;
	}
	
	clearBuffer();
	StingerClass::serial->print("pwm r:"+String(ratio)+" 1:"+String(pwmLeft)+" 2:"+String(pwmRight)+"\r");
	
	result=readBuffer();
	return result=="ACK";
}

int StingerClass::getSensor(int id){
	String result;
	
	if(id<0 || id>4){
		return 0;
	}
	
	clearBuffer();
	StingerClass::serial->print("sensor "+String(id)+"\r");
	
	result=readBuffer();
	return result.toInt();
}

bool StingerClass::setServo(int id,int position){
	String result;
	
	if(id>6 || id<1){
		//Serial.println("id error");
		return false;
	}
	
	if(position>100 || position<-100){
		//Serial.println("position error");
		return false;
	}
	
	clearBuffer();
	StingerClass::serial->print("servo "+String(id)+":"+String(position)+"\r");
	
	result=readBuffer();
	return result=="ACK";
}

bool StingerClass::setServoGPIO(int pin,int position){
	const static int convert[]={5,6,3,4,1,2};
	
	if(pin<4 || pin>9){
		//Serial.println("pin error");
		return false;
	}
	
	return setServo(convert[pin-4],position);
}

int StingerClass::getSRF04(int trigger,int output){
	String result;
	
	if(trigger<0 || trigger>12){
		return 0;
	}
	if(output<0 || output>12){
		return 0;
	}
	
	clearBuffer();
	StingerClass::serial->print("sfr04 "+String(trigger)+" "+String(output)+"\r");
	
	result=readBuffer();
	return result.toInt();
}

bool StingerClass::getSpeed(int &right,int &left){
	String result;
	clearBuffer();
	StingerClass::serial->print("vel\r");
	
	result=readBuffer();
	int space=result.indexOf(' ');
	if(space!=-1){
		result[space]='\0';
		left=atoi(&result[0]);
		right=atoi(&result[space+1]);
		return true;
	}
	return false;
}

bool StingerClass::restore(unsigned long _timeout){
	const char *validReply="Serializer, firmware 3.0.0 Copyright 2006-2010, RoboticsConnection.com\r\n>";
	const int bufferSize=128;
	char buffer[bufferSize];
	int i=0,len=strlen(validReply);
	long end;
	bool eol=false;
	
	clearBuffer();
	end=millis()+_timeout;
	StingerClass::serial->write("restore\r");
	
	if(hardwareSerial){
		((HardwareSerial*)serial)->end();
		((HardwareSerial*)serial)->begin(19200);
	}else{
		//((SoftwareSerial*)serial)->end();
		((SoftwareSerial*)serial)->begin(19200);
	}
	delay(100);
	
	while(millis()<end && i<bufferSize){
		if(StingerClass::serial->available()){
			buffer[i++]=StingerClass::serial->read();
		}
	}
	buffer[i]='\0';
	return i>=len && strcmp(&buffer[i-len],validReply)==0;
}

void StingerClass::clearBuffer(){
	while(StingerClass::serial->available())StingerClass::serial->read();
}

String StingerClass::readBuffer(){
	String result="";
	long end=millis()+timeout;
	char cIn;
	
	while(millis()<end){
		if(StingerClass::serial->available()){
			cIn=StingerClass::serial->read();
			if(cIn=='>'){
				break;
			}else if(cIn>=' ' && cIn<128){
				result+=cIn;
			}
		}
	}
	return result;
}