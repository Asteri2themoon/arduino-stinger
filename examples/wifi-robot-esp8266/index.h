#ifndef __INDEX_H__
#define __INDEX_H__


#define MULTILINE(...) #__VA_ARGS__

char *root=MULTILINE(
<html>
	<head>
		<meta charset="UTF-8">
		<title>Crunch Lab</title>
		<style>
			html, body{ 
				min-width: 100%;
			}
			div{ 
				width: 99%;
			}
			table{ 
				width: 100%;
			}
			td{ 
				width: 33%;
			}
			button{ 
				width: 100%;
				font-size: 300%;
				padding: 15%;
			}
		</style>
	</head>
	<body>
		<h1>Panneau de contrôle</h1>
		<div>
			<table>
				<tr><td></td><td><button onclick="forward()">Avancer</button></td><td></td></tr>
				<tr><td><button onclick="left()">Gauche</button></td><td></td><td><button onclick="right()">Droite</button></td></tr>
				<tr><td></td><td><button onclick="backward()">Reculer</button></td><td></td></tr>
			</table>
		</div>
		<script>
			function getXMLHttpRequest() {
				var xhr = null;
				
				if (window.XMLHttpRequest || window.ActiveXObject) {
					if (window.ActiveXObject) {
						try {
							xhr = new ActiveXObject("Msxml2.XMLHTTP");
						} catch(e) {
							xhr = new ActiveXObject("Microsoft.XMLHTTP");
						}
					} else {
						xhr = new XMLHttpRequest(); 
					}
				} else {
					alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
					return null;
				}
				
				return xhr;
			}
			function forward(){
				var xhr = getXMLHttpRequest(); // Voyez la fonction getXMLHttpRequest() définie dans la partie précédente
				xhr.open("GET", "forward", true);
				xhr.send(null);
			}
			function backward(){
				var xhr = getXMLHttpRequest(); // Voyez la fonction getXMLHttpRequest() définie dans la partie précédente
				xhr.open("GET", "backward", true);
				xhr.send(null);
			}
			function left(){
				var xhr = getXMLHttpRequest(); // Voyez la fonction getXMLHttpRequest() définie dans la partie précédente
				xhr.open("GET", "left", true);
				xhr.send(null);
			}
			function right(){
				var xhr = getXMLHttpRequest(); // Voyez la fonction getXMLHttpRequest() définie dans la partie précédente
				xhr.open("GET", "right", true);
				xhr.send(null);
			}
		</script>
	</body>
</html>
);

#endif