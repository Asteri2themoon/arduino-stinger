/**
 * Innovation Crunch Lab UTBM
 * Arthur KLIPFEL 2018
 * 
 * A sketch to control robot from a website embeded on a esp8266.
 * Datasheet: https://os.mbed.com/media/uploads/aswild/serializer_3.0_userguide.pdf
 * 
 * This sketch is made for a ESP8266 board with a stinger robot connected
 * 
 * wiring:
 * Serializer    ESP8266
 *    GND <------> GND
 *     TX <------>  4
 *     RX <------>  5
 * 
 * /!\ Do not forget to unplug the jumper near the power supply of the board to unable the TTL Serial port. /!\
 */

#include <ESP8266WiFi.h>
#include <stinger.h>
#include <SoftwareSerial.h>

#include "index.h"

SoftwareSerial swSer(4,5, false, 256);

char* validHeader="HTTP/1.1 200 Ok\r\n\r\n";

WiFiServer server(80);

void setup() {
  Serial.begin(115200);
  delay(10);
  
  if(Stinger.begin(&swSer,19200)){
    Serial.println("Stinger ok!");
  }

  Serial.println("Configuring access point...");

  WiFi.softAP("robot");

  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
  
  // Start the server
  server.begin();
  Serial.println("Server started");
}

void loop() {
  unsigned int powerLeft=0,powerRight=0;

  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  
  Serial.println("new client");
  while(!client.available()){
    delay(1);
  }
  
  String req = client.readStringUntil('\r');
  Serial.println(req);
  client.flush();
  char *result=NULL,*header=NULL;
  
  if(req.indexOf("GET / ") != -1){
    result=root;
  }
  else if(req.indexOf("GET /forward ") != -1){
    Serial.println("forward!");
    powerLeft=500;
    powerRight=500;
    header=validHeader;
  }
  else if(req.indexOf("GET /backward ") != -1){
    Serial.println("backward!");
    powerLeft=-500;
    powerRight=-500;
    header=validHeader;
  }
  else if(req.indexOf("GET /left ") != -1){
    Serial.println("left!");
    powerLeft=-100;
    powerRight=100;
    header=validHeader;
  }
  else if(req.indexOf("GET /right ") != -1){
    Serial.println("right!");
    powerLeft=100;
    powerRight=-100;
    header=validHeader;
  }
  else if(req.indexOf("GET /favicon.ico ") != -1){
    Serial.println("invalid request");
    header="HTTP/1.1 404 Not Found\r\n\r\n";
  }
  else{
    Serial.println("invalid request");
    Serial.println("send client to "+WiFi.localIP().toString());
    header=(char*)String("HTTP/1.1 303 See Other\r\nLocation: http://"+WiFi.localIP().toString()+"/\r\n\r\n").c_str();
  }
  
  client.flush();

  client.print(header);
  if(result!=NULL){
    client.print(result);
  }
  delay(1);
  Serial.println("Client disonnected");

  if(powerRight!=0 && powerLeft!=0){
    if(Stinger.move(powerRight,28,powerLeft,28)){
      Serial.println("Stinger move!");
    }
  }
}

