/**
 * Innovation Crunch Lab UTBM
 * Arthur KLIPFEL 2018
 * 
 * A sketch to move forward
 * Datasheet: https://os.mbed.com/media/uploads/aswild/serializer_3.0_userguide.pdf
 * 
 * This sketch is made for a arduino MEGA board with a stinger robot connected on Serial1
 * 
 * wiring:
 * Serializer  arduino MEGA
 *    GND <------> GND
 *     TX <------> RX1
 *     RX <------> TX1
 * 
 * /!\ Do not forget to unplug the jumper near the power supply of the board to unable the TTL Serial port. /!\
 * 
 * You could also use a arduino UNO board but out should remove all the Serial.println from the sketch and turn Stinger.begin(&Serial1,19200) to Stinger.begin(&Serial,19200)
 */


#include <stinger.h>

void setup(){
  Serial.begin(115200);
  
  delay(100);
  Serial.println("Started!");
  
  if(Stinger.begin(&Serial1,19200)){
    Serial.println("Stinger ok!");
  }
}

void loop() {
  delay(5000);
  if(Stinger.move(2500,20,2500,20)){
    Serial.println("Stinger move!");
  }
}
