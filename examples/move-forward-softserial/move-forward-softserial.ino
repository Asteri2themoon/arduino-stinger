/**
 * Innovation Crunch Lab UTBM
 * Arthur KLIPFEL 2018
 * 
 * A sketch to move forward with a software serial port
 * Datasheet: https://os.mbed.com/media/uploads/aswild/serializer_3.0_userguide.pdf
 * 
 * This sketch is made for a arduino UNO board with a stinger robot connected
 * 
 * wiring:
 * Serializer  arduino UNO
 *    GND <------> GND
 *     TX <------>  2
 *     RX <------>  3
 * 
 * /!\ Do not forget to unplug the jumper near the power supply of the board to unable the TTL Serial port. /!\
 */


#include <stinger.h>
#include <SoftwareSerial.h>

SoftwareSerial swSer(2,3);

void setup(){
  Serial.begin(115200);
  
  delay(100);
  Serial.println("Started!");
  
  if(Stinger.begin(&swSer,19200)){
    Serial.println("Stinger ok!");
  }
}

void loop() {
  delay(5000);
  if(Stinger.move(2500,20,2500,20)){
    Serial.println("Stinger move!");
  }
}
