/**
 * Innovation Crunch Lab UTBM
 * Arthur KLIPFEL 2018
 * 
 * A sketch to try all the function of a Serializer 3.0 from RoboticsConnection.com
 * Datasheet: https://os.mbed.com/media/uploads/aswild/serializer_3.0_userguide.pdf
 * 
 * This sketch is made for a arduino MEGA board with a stinger robot connected on Serial1
 * 
 * wiring:
 * Serializer  arduino MEGA
 *    GND <------> GND
 *     TX <------> RX1
 *     RX <------> TX1
 * 
 * /!\ Do not forget to unplug the jumper near the power supply of the board to unable the TTL Serial port /!\
 */


#include <stinger.h>

void setup(){
  Serial.begin(115200);
  
  delay(100);
  Serial.println("Started!");
  
  if(Stinger.begin(&Serial1,19200)){
    Serial.println("Stinger ok!");
  }
  Serial.println("version: "+Stinger.getVersion());

  if(Stinger.restore()){
    Serial.println("Stinger firmware restored!");
  }
  
  if(Stinger.setEncoderMode(QUADRATURE)){
    Serial.println("encoder ok!");
  }
  
  Serial.print("encoder mode: ");
  switch(Stinger.getEncoderMode()){
  case SINGLE:
    Serial.println("single");
    break;
  case QUADRATURE:
    Serial.println("quadrature");
    break;
  default:
    Serial.println("error");
    break;
  }

  if(Stinger.setBaudrate(115200)){
    Serial.println("baudrate ok!");
  }
  Serial.println("baudrate: "+String(Stinger.getBaudrate()));
  
  if(Stinger.setUnits(METRIC)){
    Serial.println("units ok!");
  }
  
  Serial.print("units: ");
  switch(Stinger.getUnits()){
  case METRIC:
    Serial.println("metric");
    break;
  case IMPERIAL:
    Serial.println("imperial");
    break;
  default:
    Serial.println("error");
    break;
  }
  
  for(int j=0;j<10;j++){
    if(Stinger.setIO(j,(j&1)==0)){
      Serial.println("pin "+String(j)+" to "+((j&1)==0?"low":"high"));
    }
  }
  for(int j=0;j<10;j++){
    Serial.println("pin "+String(j)+" is "+(Stinger.getIO(j)?"low":"high"));
  }
  
  if(Stinger.setBaudrate(19200)){
    Serial.println("baudrate back to 19200!");
  }
  
  delay(1000);
  if(Stinger.setServoGPIO(4,50)){
    Serial.println("servo gpio 4 to 50");
  }

  delay(2000);
  if(Stinger.setServoGPIO(4,-50)){
    Serial.println("servo gpio 4 to -50");
  }

  delay(2000);
  if(Stinger.setServoGPIO(4,0)){
    Serial.println("servo gpio 4 to 0");
  }

  delay(2000);
  if(Stinger.setServoGPIO(5,50)){
    Serial.println("servo gpio 5 to 50");
  }

  delay(2000);
  if(Stinger.setServoGPIO(5,-50)){
    Serial.println("servo gpio 5 to -50");
  }

  delay(2000);
  if(Stinger.setServoGPIO(5,0)){
    Serial.println("servo gpio 5 to 0");
  }

  delay(2000);
  
  if(Stinger.setIO(H_BRIDGES,false)){
    Serial.println("disable h bridges");
  }

  if(Stinger.setPWM(RIGHT,50)){
    Serial.println("right pwm to 50");
  }

  delay(2000);
  if(Stinger.setPWM(RIGHT,100)){
    Serial.println("right pwm to 100");
  }

  delay(2000);
  if(Stinger.setPWM(RIGHT,-100)){
    Serial.println("right pwm to -100");
  }

  delay(2000);
  if(Stinger.setPWM(RIGHT,0)){
    Serial.println("right pwm to 0");
  }

  if(Stinger.setPWM(LEFT,50)){
    Serial.println("left pwm to 50");
  }

  delay(2000);
  if(Stinger.setPWM(LEFT,100)){
    Serial.println("left pwm to 100");
  }

  delay(2000);
  if(Stinger.setPWM(LEFT,-100)){
    Serial.println("left pwm to -100");
  }

  delay(2000);
  if(Stinger.setPWM(LEFT,0)){
    Serial.println("left pwm to 0");
  }

  delay(2000);
  if(Stinger.setPWM(RIGHT,100,50)){
    Serial.println("left pwm to 50 with ratio 50");
  }

  delay(5000);
  if(Stinger.setPWM(RIGHT,0)){
    Serial.println("right pwm to 0");
  }

  delay(2000);
  if(Stinger.setPWM(100,100,50)){
    Serial.println("left pwm to 50 with ratio 50");
  }

  delay(5000);
  if(Stinger.setPWM(0,0,50)){
    Serial.println("right pwm to 0");
  }

  delay(5000);
  
  if(Stinger.setIO(H_BRIDGES,false)){
    Serial.println("disable h bridges");
  }

  PID pid={10,0,0,25};
  if(Stinger.setSpeedPID(pid)){
    Serial.println("set speed pid to P="+String(pid.p)+" I="+String(pid.i)+" D="+String(pid.d)+" L="+String(pid.l));
  }
  
  PID pid2={0,0,0,0};
  pid2=Stinger.getSpeedPID();
  Serial.println("speed pid is P="+String(pid2.p)+" I="+String(pid2.i)+" D="+String(pid2.d)+" L="+String(pid2.l));
  
  pid={1,0,0,10};
  if(Stinger.setPositionPID(pid)){
    Serial.println("set position pid to P="+String(pid.p)+" I="+String(pid.i)+" D="+String(pid.d)+" A="+String(pid.a));
  }
  
  pid2={0,0,0,0};
  pid2=Stinger.getPositionPID();
  Serial.println("position pid is P="+String(pid2.p)+" I="+String(pid2.i)+" D="+String(pid2.d)+" A="+String(pid2.a));
  
  if(Stinger.resetPositionPID()){
    Serial.println("position pid reseted to default value");
  }
  
  pid2={0,0,0,0};
  pid2=Stinger.getPositionPID();
  Serial.println("position pid is P="+String(pid2.p)+" I="+String(pid2.i)+" D="+String(pid2.d)+" A="+String(pid2.a));

  Serial.println("is PID busy? "+String(Stinger.isPIDBusy()));
  

  for(int j=0;j<=10;j++){
    if(Stinger.setMotorsSpeed(j*10-50,j*10-50)){
      Serial.println("motor to speed "+String(j*10-50));
    }

    delay(1000);
    if(Stinger.stopMotors()){
      Serial.println("motor stopped");
    }

    delay(1000);
  }


  if(Stinger.move(2500,20,2500,20)){
    Serial.println("Stinger move!");
  }


  Serial.println("is PID busy? "+String(Stinger.isPIDBusy()));
  delay(3000);
  

  if(Stinger.blink(50)){
    Serial.println("led 1 ok!");
  }
  delay(3000);
  if(Stinger.blink(100)){
    Serial.println("led 1 ok!");
  }

  delay(3000);
  if(Stinger.blink(0)){
    Serial.println("led 1 ok!");
  }
}
int i=0;
void loop() {
  if(((i++)&0x7)==0){
    if(Stinger.clearEncoders()){
      Serial.println("encoder cleared!");
    }
  }
  int right,left;
  if(Stinger.getEncoders(right,left)){
    Serial.println("right:"+String(right)+" ; left:"+String(left));
  }
  for(int j=0;j<=4;j++){
    Serial.print("sensor "+String(j)+":"+String(Stinger.getSensor(j))+"   ");
  }
  Serial.println("");
  if(Stinger.getSpeed(right,left)){
    Serial.println("speed: right="+String(right)+" left="+String(left));
  }
  
  delay(1000);
}
